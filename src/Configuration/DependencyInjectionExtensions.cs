using System;
using BuildingBlocks.MessageBus.src.Messages;
using Microsoft.Extensions.DependencyInjection;

namespace BuildingBlocks.MessageBus.src.Configuration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddMessageBus(this IServiceCollection services, string connection)
        {
            if (string.IsNullOrEmpty(connection)) throw new ArgumentNullException();

            services.AddSingleton<IMessageBus>(new MessageBus(connection));

            return services;
        }
    }
}